from conans import CMake, ConanFile, tools


class SshtConan(ConanFile):
    name = "ssht"
    version = "1.2b1"
    license = "GPL-2.0"
    url = "https://gitlab.com/mdavezac-conan/ssht"
    homepage = "https://github.com/astro-informatics/ssht"
    description = "Fast spin spherical harmonic transforms"
    settings = "os", "arch", "compiler", "build_type"
    topics = ("Physics", "Astrophysics", "Radio Interferometry")
    options = {"conan_fftw": [True, False]}
    default_options = "conan_fftw=False"
    generators = "cmake"

    def source(self):
        git = tools.Git(folder="ssht")
        git.clone(self.homepage, branch="public")
        git.checkout("7378ce8")
        # This small hack might be useful to guarantee proper /MT /MD linkage
        # in MSVC if the packaged project doesn't have variables to set it
        # properly
        tools.replace_in_file(
            "ssht/CMakeLists.txt",
            "project(ssht C)",
            """project(ssht C)
               include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
               conan_basic_setup()
            """,
        )
        tools.replace_in_file(
            "ssht/CMakeLists.txt",
            "target_include_directories(ssht PUBLIC ${FFTW3_INCLUDE_DIRS})",
            """target_include_directories(ssht PUBLIC
                ${FFTW3_INCLUDE_DIRS}
                INTERFACE
                ${FFTW3_INCLUDE_DIRS}
                $<INSTALL_INTERFACE:include>)
            """
        )
        tools.replace_in_file(
          "ssht/src/c/ssht_test.c",
          "#include <omp.h>",
          ""
        )

    def requirements(self):
        if self.options.conan_fftw:
            self.requires("fftw/3.3.8@bincrafters/stable")

    def configured_cmake(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_POSITION_INDEPENDENT_CODE"] = True
        cmake.definitions["CMAKE_DISABLE_FIND_PACKAGE_OpenMP"] = True
        cmake.definitions["openmp"] = False
        cmake.definitions["tests"] = False
        cmake.configure(source_folder="ssht")
        return cmake

    def build(self):
        cmake = self.configured_cmake()
        cmake.build()

    def package(self):
        cmake = self.configured_cmake()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["ssht"]
