from cpt.packager import ConanMultiPackager


if __name__ == "__main__":
    builder = ConanMultiPackager()
    builder.add_common_builds()
    builder.builds = [
        [build[0], {"ssht:conan_fftw": True}, build[2], build[3]]
        for build in builder.builds
    ]

    builder.run()
